package dawid.siwko.sejm;

/**
 * Created by dawid on 11.01.17.
 */
public enum Attribute {
    CostsSum,
    CostsOfficeRepair,
    CostsAverage,
    TripsMaxTimesAbroad,
    TripsMaxDaysAbroad,
    TripsMaxCost,
    TripsCountry
}
